package com.javagda14.events.service;

import com.javagda14.events.dao.UserDao;
import com.javagda14.events.model.AppUser;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.Optional;

@LocalBean
@Singleton
public class UserService {
    @EJB
    private UserDao userDao;

    public boolean checkUsernameNotExists(String username) {
        return true;
    }

    public boolean checkEmailNotExists(String email) {
        return true;
    }

    public void register(AppUser appUser) {
        userDao.add(appUser);
    }

    public Optional<Long> login(String login, String password) {
        Optional<AppUser> appUserOptional=userDao.getUserWithLoginAndPassword(login,password);
        if (appUserOptional.isPresent()){
            return Optional.of(appUserOptional.get().getId());
        }return Optional.empty();
    }
}
