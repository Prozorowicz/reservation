package com.javagda14.events.dao;

import com.javagda14.events.model.AppUser;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@LocalBean
@Singleton
public class UserDao {
    public void add(AppUser newUser) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(newUser);
            transaction.commit();
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
    }

    public List<AppUser> getAllUsers() {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            List<AppUser> list = session.createQuery("from AppUser ", AppUser.class).list();
            return list;
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
        return new ArrayList<>();
    }


    public void removeUserWithId(int id) {
        Optional<AppUser> userOpt = findUserWithId(id);
        if (userOpt.isPresent()) {
            AppUser user = userOpt.get();
            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();
                session.remove(user);
                transaction.commit();
            } catch (SessionException sessionException) {
                transaction.rollback();
                System.out.println("Error");
            }
        }
    }

    public Optional<AppUser> findUserWithId(int id) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query<AppUser> query = session.createQuery("from AppUser where id= :id", AppUser.class);
            query.setParameter("id", id);
            AppUser result = query.uniqueResult();
            return Optional.of(result);
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
        return Optional.empty();
    }

    public void updateUser(AppUser userToModify) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            session.saveOrUpdate(userToModify);

            transaction.commit();
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }

    }

    public Optional<AppUser> getUserWithLoginAndPassword(String login, String password) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query<AppUser> query = session.createQuery
                    ("from AppUser where username= :username and password = :password", AppUser.class);
            query.setParameter("username", login);
            query.setParameter("password", password);
            AppUser result = query.uniqueResult();
            return Optional.ofNullable(result);
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
        return Optional.empty();
    }
}
