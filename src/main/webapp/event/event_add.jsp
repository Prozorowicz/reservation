<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 08/10/2018
  Time: 18:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event Add</title>
</head>
<body>
<%@include file="/header.jsp"%>
<h2>Event Add:</h2>
<span style="color: red; ">
<c:out value="${error_message}"/>
</span>

<form action="/event/add" method="post">
    <input name="owner_id" type="text" value="<c:out value="${sessionScope.logged_id}"/>">
    <div>
        <label for="name">Name:</label>
        <input id="name" name="name" type="text">
    </div>
    <div>
        <label for="description">Description:</label>
        <input id="description" name="description" type="text">
    </div>
    <div>
        <label for="time">Time:</label>
        <input id="time" name="time" type="datetime-local">
    </div>
    <div>
        <label for="lenght">Lenght:</label>
        <input id="lenght" name="lenght" type="number">
    </div>
    <input type="submit" value="Add">
</form>

</body>
</html>

