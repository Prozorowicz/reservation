
<table style="width: 100%;">
    <tr>
        <td><a href="/">Home</a> </td>
        <td><a href="/user/register">Register</a> </td>
        <td><a href="/login">Login</a> </td>
        <c:if test="${not empty sessionScope.logged_username}">
            <td><a href="/event/add">Add Event</a> </td>
            <td><a href="/logout">Logout</a> </td>
        </c:if>
    </tr>
</table>
<c:if test="${not empty sessionScope.logged_username}">
    Hello <c:out value="${sessionScope.logged_username}"/>
</c:if>